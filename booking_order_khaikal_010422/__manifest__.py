# -*- coding: utf-8 -*-
{
    'name': "Booking Order",

    'summary': """
        Custom Module untuk testing Hashmicro""",

    'description': """
        Custom Module untuk testing Hashmicro
    """,

    'author': "Muhamad Khaikal Azhari",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
        'wizards/reason_cancel.xml',
        'security/ir.model.access.csv',
        'views/booking_order.xml',
        'views/service_team.xml',
        'views/work_order.xml',
        'views/menu.xml',

        'data/ir_sequence_data.xml',
        'reports/work_order_report.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
