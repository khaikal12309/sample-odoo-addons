 
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
 
class ReasonCancel(models.TransientModel):
	_name = 'reason.cancel.wizard'

	work_order_id = fields.Many2one(comodel_name='booking_order_khaikal_010422.work_order')
	note = fields.Text(string="Reason")
	
	def action_process(self):
		work_order = self.work_order_id
		
		work_order.sudo().write({
			'state':'canceled',
			'note': self.note
		})

		return True