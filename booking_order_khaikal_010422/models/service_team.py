# -*- coding: utf-8 -*-

from odoo import models, fields, api


class booking_order_khaikal_010422_service_team(models.Model):
    _name = 'booking_order_khaikal_010422.service_team'
    _description = 'booking_order_khaikal_010422.service_team'

    name = fields.Char(string='Team Name', required=True)
    leader_id = fields.Many2one(string='Team Leader', comodel_name='res.users', required=True)
    member_ids = fields.Many2many(string='Team Members',  comodel_name='res.users')