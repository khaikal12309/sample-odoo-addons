
from odoo import models, fields, api
from datetimerange import DateTimeRange
from odoo.exceptions import ValidationError, RedirectWarning

class SaleOrderInherit(models.Model):
    _inherit = "sale.order"

    is_booking_order = fields.Boolean(string='Booking Order', default=False)
    service_team_id = fields.Many2one(comodel_name='booking_order_khaikal_010422.service_team', string='Team')
    leader_id = fields.Many2one(string='Team Leader', comodel_name='res.users')
    member_ids = fields.Many2many(string='Team Members',  comodel_name='res.users')
    booking_start = fields.Datetime(string='Booking Start')
    booking_end = fields.Datetime(string='Booking End')

    workorder_ids = fields.One2many(comodel_name='booking_order_khaikal_010422.work_order', inverse_name='order_id')
    workorder_count = fields.Integer(string='Work Order', compute='count_workorder')
    
    # Compute Count
    @api.depends('workorder_ids')
    def count_workorder(self):
        for rec in self:
            rec.workorder_count = len(rec.workorder_ids)
    
    # Action View Work Order
    def action_view_workorder(self):
        workorders = self.mapped('workorder_ids')
        action = self.env["ir.actions.actions"]._for_xml_id("booking_order_khaikal_010422.work_order_action_window")
        if len(workorders) > 1:
            action['domain'] = [('id', 'in', workorders.ids)]
        elif len(workorders) == 1:
            form_view = [(self.env.ref('booking_order_khaikal_010422.work_order_view_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
            else:
                action['views'] = form_view
            action['res_id'] = workorders.id
        else:
            action = {'type': 'ir.actions.act_window_close'}

        context = {}
        if len(self) == 1:
            context.update({
                'default_service_team_id': self.service_team_id.id,
                'default_leader_id': self.leader_id.id,
            })
        action['context'] = context
        return action

    @api.onchange('service_team_id')
    def _onchange_service_team_id(self):
        self.leader_id = self.service_team_id.leader_id
        self.member_ids |= self.service_team_id.member_ids

    def intersection(self):
        overlap = False
        work_order = False
        time_range1 = DateTimeRange(self.booking_start, self.booking_end)
        work_order_exists = self.env['booking_order_khaikal_010422.work_order'].sudo().search(['|',('service_team_id','=', self.service_team_id.id),('leader_id','=', self.leader_id.id),('state','!=','canceled')])
        if work_order_exists:
            for rec in work_order_exists:
                time_range2 = DateTimeRange(rec.planned_start, rec.planned_end)
                intersection = str(time_range1.intersection(time_range2))
                if intersection != 'NaT - NaT':
                    overlap = True
                    work_order = rec                    
        return overlap, work_order

    def action_check(self):
        overlap, workorder = self.intersection()
        if overlap:
            raise ValidationError("Team already has work order during that period on {}".format(workorder.order_id.name))
        else:
            raise ValidationError("Team is available for booking")
    
    def action_confirm(self):
        overlap, workorder = self.intersection()
        if overlap:
            raise ValidationError("Team already has work order during that period on {}".format(workorder.order_id.name))
        else:
            res = super(SaleOrderInherit, self).action_confirm()
            vals_wo = {
                'order_id' : self.id,
                'service_team_id': self.service_team_id.id,
                'leader_id': self.leader_id.id,
                'member_ids': self.member_ids,
                'planned_start': self.booking_start,
                'planned_end': self.booking_end
            }
            workorder = self.env['booking_order_khaikal_010422.work_order'].sudo().create(vals_wo)
            return res