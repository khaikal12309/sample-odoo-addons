# -*- coding: utf-8 -*-

from email.policy import default
from odoo import models, fields, api, models, _


class booking_order_khaikal_010422_work_order(models.Model):
    _name = 'booking_order_khaikal_010422.work_order'
    _description = 'booking_order_khaikal_010422.work_order'

    name = fields.Char(string='WO Number', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    order_id = fields.Many2one(comodel_name='sale.order')
    service_team_id = fields.Many2one(comodel_name='booking_order_khaikal_010422.service_team', string='Team', required=True)
    leader_id = fields.Many2one(string='Team Leader', comodel_name='res.users', required=True)
    member_ids = fields.Many2many(string='Team Members',  comodel_name='res.users')
    planned_start = fields.Datetime(string='Planned Start', required=True)
    planned_end = fields.Datetime(string='Planned End', required=True)
    date_start = fields.Datetime(string='Date Start', readonly=True)
    date_end = fields.Datetime(string='Date End', readonly=True)
    state = fields.Selection(selection=[('pending','Pending'), ('in_progress','In Progress'), ('done','Done'), ('canceled','Canceled')], default="pending")
    note = fields.Text(string='Text')
    # Extra field for measure in view Pivot, Graph
    duration = fields.Integer(string='Duration', compute='_compute_duration', store=True, default=0)
    
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('booking_order_khaikal_010422.work_order') or _('New')
        result = super(booking_order_khaikal_010422_work_order, self).create(vals)
        return result

    @api.depends('date_start','date_end')
    def _compute_duration(self):
        for rec in self:
            if rec.date_end and rec.date_start:
                different_date = rec.date_end - rec.date_start
                rec.duration = int(different_date.days)

    def action_start(self):
        self.date_start = fields.datetime.now()
        self.state = 'in_progress'

    def action_end(self):
        self.date_end = fields.datetime.now()
        self.state = 'done'
    
    def action_reset(self):
        self.date_start = False
        self.state = 'pending'
    
    @api.onchange('service_team_id')
    def _onchange_service_team_id(self):
        self.leader_id = self.service_team_id.leader_id
        self.member_ids |= self.service_team_id.member_ids
        